import React from 'react';
import {connect} from 'react-redux'
import moment from 'moment';
//ToDo: split into separate files
class Account extends React.Component {
    render() {
        return (
            <section id="account-section">
                <RoomList collection={this.props.data}/>
            </section>
        );
    }
}

class RoomList extends React.Component {
    render() {
        return (
            <div>
                {this.props.collection ? this.props.collection.map(function (room) {
                    return (<ThingList key={room.name} collection={room.things}/>);
                }) : "No data"}
            </div>
        );
    }
}
class ThingList extends React.Component {
    render() {
        return (
            <div>
                {this.props.collection ? this.props.collection.map(function (thing) {
                    return (<Parameter data={thing} key={thing.name}/>);
                }) : "No data"}
            </div>

        )
    }
}


class Parameter extends React.Component {
    render() {
        return (
            <div>
                {this.props.data.parameters ? this.props.data.parameters.map(function (parameter) {
                    if (parameter.set !== "false") {
                        return (<ParameterSet key={parameter.name} item={parameter}/>);
                    } else {
                        return ( <ParameterGet key={parameter.name} item={parameter}/> );
                    }
                }) : "No parameters"}</div>
        )
    }
}


class ParameterSet extends React.Component {
    render() {
        return (<div className="parameter" id={this.props.item.name}></div>);
    }

}


class ParameterGet extends React.Component {
    render() {
        return (<div>
            <div className='nameAndValue'> {this.props.item.name} :<b> {this.props.item.value} </b></div>
            <div className='date'> {moment(this.props.item.time * 1000).format('MMMM Do YYYY, h:mm:ss')} </div>
        </div>);
    }
}


const mapStateToProps = function (state) {
    return {data: state.user}
};


export default connect(mapStateToProps)(Account);


