import React from 'react';
import {connect} from 'react-redux'
import {getPublicTemperature} from '../actions/publicActions'

class PublicTemperature extends React.Component {
    componentDidMount() {
        this.props.dispatch(getPublicTemperature());
    }

    render() {
        return (
            <section className="temperature-section">
                <b>{this.props.temperature}</b>
                {this.props.isFetching ? <b>Please wait....</b> : null}
                {this.props.error ? <b>{this.props.error.message}</b> : null}
            </section>);
    }
}
const mapStateToProps = function (state) {
    return {...state.publicTemperature};
};

export default  connect(mapStateToProps)(PublicTemperature);


