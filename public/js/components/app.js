import React from 'react';
import {Link} from 'react-router'
import {connect} from 'react-redux'
import {logout} from '../actions/accountActions'
class App extends React.Component {

    handleLogoutClick() {
        this.props.dispatch(logout());
    }

    render() {
        return (
            <div>
                <section className="top-header">
                    <Link to="/" className="intelligent-house">
                        <img src="images/home.png" width="60" height="50"/>
                        Intelligent House
                    </Link>

                    <div className="log-container">
                        {
                            this.props.user ?
                                <Link to="/account">
                                    <button className="register button-default"> Account</button>
                                </Link> :
                                <Link to="/login">
                                    <button className="register button-default"> Log in</button>
                                </Link>
                        }
                        < Link to="/register">
                            <button className="register button-default"> Register</button>
                        </Link>

                        {this.props.user ?
                            <button className="log-out button-default" onClick={this.handleLogoutClick.bind(this)}>Log
                                out</button> : null}
                    </div>
                </section>
                <section className="body-container">
                    {this.props.children}
                </section>
                <section className="footer">
                    <Link to="/public-temperature" className="img-temperature">
                        <img src="images/thermometer.png" width="50" height="50"/>
                    </Link>
                    © 2016 Intelligent House
                </section>
            </div>
        );
    }
}

const mapStateToProps = function (state) {
    return {user: state.user};
};


export default connect(mapStateToProps)(App);


