import React from 'react';
import {connect} from 'react-redux'
import {authenticate} from '../actions/accountActions'

import ReactPhoneInput from 'react-phone-input';

class LogIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {phoneNumber: "", password: ""};
    }

    handlePhoneChange(value) {
        this.setState({phoneNumber: value});
    }

    handlePasswordInputChange(event) {
        const value = event.target.value;
        const propName = event.target.name;
        const newState = Object.assign({}, this.state, {[propName]: value});

        this.setState(newState);
    }

    handleLogInClick(e) {
        e.preventDefault();
        const validationResult = this.validate();
        if (validationResult.success) {
            this.props.dispatch(authenticate(this.state.phoneNumber, this.state.password, true));
        }
        else {
            alert(validationResult.errors.join('\n'));
        }
    }

    validate() {
        var errors = [];
        const {phoneNumber, password} = this.state;
        if (!phoneNumber) errors.push('Please enter phone number!');
        if (!password) errors.push('Please enter password!');
        return {success: !Boolean(errors.length), errors: errors};
    }

    render() {
        return (
            <section id="login-section">
                <form onSubmit={this.handleLogInClick.bind(this)}>
                    <div className="register-container">
                        <div >
                            <ReactPhoneInput
                                defaultCountry={'ua'}
                                value={this.state.phoneNumber}
                                onChange={this.handlePhoneChange.bind(this)}
                            />
                        </div>
                        <br/>
                        <input className="input-text password"
                               name="password"
                               onChange={this.handlePasswordInputChange.bind(this)}
                               value={this.state.password}
                               type="password" size="40"
                               placeholder="Password"/>
                        <br/>
                        <input type="submit"
                               className="waves-effect waves-light btn" disabled={this.props.isFetching}
                               value={"Sign in"}/>

                        {this.props.error ? this.props.error.message : ''}
                    </div>
                </form>
            </section>);
    }
}

const mapStateToProps = function (state) {
    const {isFetching, error} = state.authenticate;

    return {isFetching, error}
};

const RegisterComponent = connect(mapStateToProps)(LogIn);

export default RegisterComponent;


