import React from 'react';
import {connect} from 'react-redux'
import {register} from '../actions/accountActions'

import ReactPhoneInput from 'react-phone-input';
import InputPassword from 'react-ux-password-field';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {phoneNumber: "", password: "", repeatPassword: ""};
    }

    handlePhoneChange(value) {
        this.setState({phoneNumber: value});
    }

    handlePasswordInputChange(value) {
        const newState = Object.assign({}, this.state, {password: value});

        this.setState(newState);
    }

    handleRepeatPasswordInputChange(value) {
        const newState = Object.assign({}, this.state, {repeatPassword: value});

        this.setState(newState);
    }

    handleSignUpClick(e) {
        e.preventDefault();
        const validationResult = this.validate();
        if (validationResult.success) {
            this.props.dispatch(register(this.state.phoneNumber, this.state.password));
        }
        else {
            alert(validationResult.errors.join('\n'));
        }
    }

    validate() {
        var errors = [];
        const {phoneNumber, password, repeatPassword} = this.state;
        if (!phoneNumber) errors.push('Phone number is required!');
        if (!password) errors.push('Password is required!');
        if (!repeatPassword) errors.push('Repeat password is required!');
        if (password != repeatPassword) errors.push('Passwords do not match!');
        return {success: !Boolean(errors.length), errors: errors};
    }

    render() {
        return (
            <section id="register-section">
                <form onSubmit={this.handleSignUpClick.bind(this)}>
                    <div className="register-container">
                        <div className="phone-container">
                            <ReactPhoneInput
                                defaultCountry={'ua'}
                                value={this.state.phoneNumber}
                                onChange={this.handlePhoneChange.bind(this)}
                            />
                        </div>
                        <br/>
                        <InputPassword className="input-text"
                                       onChange={this.handlePasswordInputChange.bind(this)}
                                       value={this.state.password}
                                       placeholder="Password"
                                       toggleMask={false}/>
                        <br/>
                        <InputPassword className="input-text"
                                       onChange={this.handleRepeatPasswordInputChange.bind(this)}
                                       value={this.state.repeatPassword}
                                       placeholder="Repeat password"
                                       toggleMask={false}/>
                        <br/>
                        <input className="btn-default signUp" disabled={this.props.isFetching}
                               type="submit" value={"Sign up"}/>

                        {this.props.error ? this.props.error.message : ''}
                    </div>
                </form>
            </section>);
    }
}

const mapStateToProps = function (state) {
    const {isFetching, error} = state.register;

    return {isFetching, error}
};

const RegisterComponent = connect(mapStateToProps)(Register);

export default RegisterComponent;


