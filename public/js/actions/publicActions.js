import * as types from '../constants/actionTypes';
import fetch from 'isomorphic-fetch';

function publicTempRequest() {
    return {
        type: types.GET_TEMPERATURE_REQUEST
    }
}
function publicTempSuccess(response) {
    return {
        type: types.GET_TEMPERATURE_SUCCESS,
        temperature: response
    }
}
function publicTempError(error) {
    return {
        type: types.GET_TEMPERATURE_ERROR,
        error: error
    }
}

export function getPublicTemperature() {
    return dispatch => {
        dispatch(publicTempRequest());
        return fetch(`http://intelligenthouse-andstepkotest.rhcloud.com/intelligent-house-api/public_temperature`)
            .then(response => response.json())
            .then(json=>dispatch(publicTempSuccess(json)))
            .catch(error => dispatch(publicTempError(error)));
    }
}