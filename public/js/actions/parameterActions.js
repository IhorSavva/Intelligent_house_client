import * as types from '../constants/actionTypes';
import fetch from 'isomorphic-fetch';

export function setParameter(gateway, parameter, value) {
    return dispatch => {
        dispatch(setParameterRequest());
        return fetch(`http://intelligenthouse-andstepkotest.rhcloud.com/intelligent-house-api/parameter`,
            {
                method: "POST",
                body: {gateway_url: `${gateway}/${parameter}`, transaction: value}
            })
            .then(response => {
                if (response.status == 401) throw new Error("Invalid credentials");
                dispatch(setParameterSuccess())
            })
            .catch(error => dispatch(setParameterError(error)));
    }
}
function setParameterRequest() {
    return {
        type: types.SET_PARAMETER_REQUEST
    }

}
function setParameterSuccess() {
    return {
        type: types.SET_PARAMETER_SUCCESS
    }

}
function setParameterError(error) {
    return {
        type: types.SET_PARAMETER_ERROR,
        error
    }

}

export function refreshThings(gateway) {
    return dispatch => {
        dispatch(refreshThingsRequest());
        return fetch(`http://intelligenthouse-andstepkotest.rhcloud.com/intelligent-house-api/refresh`, {
            method: "POST",
            body: {gateway_url: gateway}
        })
            .then(response => {
                if (response.status == 401) throw new Error("Invalid credentials");
                return response.json();
            })
            .then(json=> {
                dispatch(refreshThingsSuccess(json))
            })
            .catch(error => dispatch(refreshThingsError(error)));
    }
}
function refreshThingsRequest() {
    return {
        type: types.REFRESH_THINGS_REQUEST
    }

}
function refreshThingsSuccess(response) {
    return {
        type: types.REFRESH_THINGS_SUCCESS,
        response
    }

}
function refreshThingsError(error) {
    return {
        type: types.REFRESH_THINGS_ERROR,
        error
    }

}