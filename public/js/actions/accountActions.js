import * as types from '../constants/actionTypes';
import fetch from 'isomorphic-fetch';
import {push} from 'react-router-redux';

export function authenticate(login, password, redirect) {
    return dispatch => {
        dispatch(requestAuthenticate(login, password));
        return fetch(`http://intelligenthouse-andstepkotest.rhcloud.com/intelligent-house-api/state?login=${login.replace('+', '')}&password=${password}`)
            .then(response => {
                if (response.status == 401) throw new Error("Invalid credentials");
                return response.json();
            })
            .then(json=> {
                window.sessionStorage.setItem('user_login', login);
                window.sessionStorage.setItem('user_password', password);
                dispatch(successAuthenticate(json));
                if (redirect) dispatch(push('/account'))
            })
            .catch(error => dispatch(errorAuthenticate(error)));
    }
}

function requestAuthenticate(login, password) {
    return {
        type: types.AUTHENTICATE_REQUEST,
        login, password
    }
}

function successAuthenticate(response) {
    return {
        type: types.AUTHENTICATE_SUCCESS,
        user: response,
        redirect: '/account'
    }
}

function errorAuthenticate(error) {
    return {
        type: types.AUTHENTICATE_ERROR,
        user: null, error: error
    }
}

export function register(login, password) {
    return dispatch => {
        dispatch(requestRegister(login, password));
        return fetch(`http://intelligenthouse-andstepkotest.rhcloud.com/intelligent-house-api/register?${login}&password=${password}`)
            .then(response => dispatch(successRegister(response)))
            .catch(error => dispatch(errorRegister(error)));
    }
}

function requestRegister(login, password) {
    return {
        type: types.REGISTER_REQUEST,
        login, password
    }
}

function successRegister(response) {
    return {
        type: types.REGISTER_SUCCESS,
        user: response
    }
}

function errorRegister(error) {
    return {
        type: types.REGISTER_ERROR,
        user: null,
        error: error
    }
}

export function logout() {
    return {
        type: types.LOGOUT,
        user: null,
    }
}