export default {
    backspace: 8,
    tab: 9,
    escape: 27,
    arrows: {left: 37, up: 38, right: 39, down: 40},
    delete: 46,
    letters: {
        a: 65,
        c: 67
    },
    digits: [48, 49, 50, 51, 52, 53, 54, 55, 56, 57],
    numDigits: [96, 97, 98, 99, 100, 101, 102, 103, 104, 105]
};