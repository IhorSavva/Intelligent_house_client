import React from 'react'

import Index from './index.js';
import Account from '../components/account.js';
import PublicTemperature from '../components/public-temperature.js';
import Register from '../components/register.js';
import LogIn from '../components/login.js';
import App from '../components/app.js';
import NoMatch from './notFound'
import {Router, Route, IndexRoute} from 'react-router'

class router extends React.Component {
    render() {
        return (<Router history={this.props.history}>
            <Route path="/" component={App}>
                <IndexRoute component={Index}/>
                <Route path="/account" component={Account}/>
                <Route path="/login" component={LogIn}/>
                <Route path="/public-temperature" component={PublicTemperature}/>
                <Route path="/register" component={Register}/>
                <Route path="*" component={NoMatch}/>
            </Route>
        </Router>);
    }
}

export default router;
