import React from 'react'
import {render} from 'react-dom'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux'
import browserHistory from './history'
import reducer from './reducers'
import Router from './containers/router'
import {authenticate} from './actions/accountActions'

const middleware = [thunk,  routerMiddleware(browserHistory)];

const store = createStore(
    reducer,
    applyMiddleware(...middleware)
);
const history = syncHistoryWithStore(browserHistory, store);

const user_login = window.sessionStorage.getItem('user_login');
const user_password = window.sessionStorage.getItem('user_password');

if(user_login && user_password) store.dispatch(authenticate(user_login, user_password));
render(
    <Provider store={store}>
        <Router history={history}/>
    </Provider>,
    document.getElementById('main-content')
);
