import {combineReducers} from 'redux'
import * as accountReducers from './account'
import * as publicReducers from './public'
import {routerReducer} from 'react-router-redux';

const combinedReducer = combineReducers({...accountReducers, ...publicReducers, routing: routerReducer});

export default combinedReducer;