import * as types from '../constants/actionTypes';

export function register(state = {isFetching: false, error: null}, action) {
    switch (action.type) {
        case types.REGISTER_REQUEST:
            return {
                ...state,
                isFetching: true,
                error: null
            };
        case types.REGISTER_SUCCESS:
            return {
                ...state,
                isFetching: false,
                error: null
            };
        case types.REGISTER_ERROR:
            return {
                ...state,
                isFetching: false,
                error: action.error
            };
        default:
            return state
    }
}

export function user(state = null, action) {
    switch (action.type) {
        case types.AUTHENTICATE_SUCCESS:
            return action.user;
        case types.LOGOUT:
            return null;
        default:
            return state
    }
}

export function authenticate(state = {isFetching: false, error: null}, action) {
    switch (action.type) {
        case types.AUTHENTICATE_REQUEST:
            return {
                ...state,
                isFetching: true,
                error: null
            };
        case types.AUTHENTICATE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                error: null
            };
        case types.AUTHENTICATE_ERROR:
            return {
                ...state,
                isFetching: false,
                error: action.error
            };
        default:
            return state
    }
}