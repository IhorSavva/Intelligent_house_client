import * as types from '../constants/actionTypes';

export function publicTemperature(state = {isFetching: false, error: null, temperature: null}, action) {
    switch (action.type) {
        case types.GET_TEMPERATURE_REQUEST:
            return {
                ...state,
                isFetching: true,
                error: null,
                temperature: null
            };
        case types.GET_TEMPERATURE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                error: null,
                temperature: action.temperature
            };
        case types.GET_TEMPERATURE_ERROR:
            return {
                ...state,
                isFetching: false,
                error: action.error,
                temperature: null
            };
        default:
            return state
    }
}

