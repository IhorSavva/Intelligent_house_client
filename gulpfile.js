var gulp = require('gulp');
var inject = require('gulp-inject');
var fs = require('fs');

var uglify = require('gulp-uglify');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
var buffer = require('vinyl-buffer');
var runSequence = require('gulp-run-sequence');

var browserify = require('browserify');
var source = require('vinyl-source-stream');
var babelify = require("babelify");

var util = require('gulp-util');

var clean = require('gulp-clean');

var _ = require('lodash');

var viewsDirectory = './views/';
var publicJsDir = './public/js/';

gulp.task('clean', ()=> {
    return gulp.src(`${publicJsDir}/build.js`).pipe(clean({force: true}));
});

gulp.task('build-public', ()=> {
    var publicScripts = fs.readdirSync(publicJsDir)
        .filter(item=>item != 'vendor')
        .map(item=>item.indexOf('.js') > -1 ? publicJsDir + item : publicJsDir + item + '/**/*.js');

    return gulp.src(publicScripts)
        .pipe(sourcemaps.init())
        .pipe(babel({presets: ['es2015', 'react']}))
        .pipe(concat('build.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(publicJsDir));
});


gulp.task('inject-public', ()=> {
    var target = gulp.src(viewsDirectory + 'index.hbs');

    var sources = gulp.src([publicJsDir + 'vendors.js', publicJsDir + 'build.js'], {read: false});

    return target
        .pipe(inject(sources, {ignorePath: 'public'}))
        .pipe(gulp.dest(viewsDirectory));
});


gulp.task('inject', (cb)=> {
    runSequence('clean', ['vendor', 'bundle'], 'inject-public', cb);
});

gulp.task('watch', ()=> {
    gulp.watch([`./public/**/*.js`], ['bundle']);
});


gulp.task('default', ['inject']);

function getNPMPackages() {
    var packageManifest = {};
    try {
        packageManifest = require("./package.json");
    } catch (e) {
    }

    return _.keys(packageManifest.dependencies) || [];
}

function handleError() {
    var args = Array.prototype.slice.call(arguments);
    console.log("errors: ", args);
    this.emit("end");
}


gulp.task("vendor", function () {
    var npmPackages = getNPMPackages();
    return browserify()
        .require(npmPackages)
        .bundle()
        .on("error", handleError)
        .pipe(source("vendors.js"))
        .pipe(gulp.dest(publicJsDir));
});

gulp.task("bundle", function () {
    var npmPackages = getNPMPackages();

    return browserify("./public/js/index.js")
        .external(npmPackages)
        .transform(babelify)
        .bundle().on("error", handleError)
        .pipe(source("build.js"))
        .pipe(gulp.dest(publicJsDir));

});
